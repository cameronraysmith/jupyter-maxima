# Description

This project contains a Dockerfile to build an image based on [Jupyter Notebook Data Science Stack](https://hub.docker.com/r/jupyter/datascience-notebook/) that includes [Maxima](http://maxima.sourceforge.net) Jypyter kernel from [Robert Dodier](https://github.com/robert-dodier).

# Build and run with [docker](https://docs.docker.com/) only

## Build
1. `git clone git@gitlab.com:alessandro.dibella/docker/jupyter-maxima.git`
1. `cd jupyter-maxima`
1. `docker build -t alessandro.dibella/jupyter-maxima:latest .`

## Run
`docker run -it --rm -p 8888:8888 -v ${HOME}:/home/jovyan/notebooks alessandro.dibella/jupyter-maxima:latest start-notebook.sh --NotebookApp.token= --Session.key=b''`

# Build and run with [docker compose](https://docs.docker.com/compose/)

`docker-compose up` to run in foreground or `docker-compose up -d` to run in background.

# Usage
Open the [http://localhost:8888/tree?] to access the [Jupyter](http://jupyter.org/) interface showing the home directory of the current user.

When creating a new notebook, the `Maxima` option will be available. See [MaximaJupyterExample.ipynb](https://github.com/robert-dodier/maxima-jupyter/blob/master/MaximaJupyterExample.ipynb) as an example.
