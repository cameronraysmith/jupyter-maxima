# Distributed under the terms of the Modified BSD License.
FROM jupyter/datascience-notebook


USER root

RUN apt-get update && \
	apt-get -y remove maxima maxima-doc wxmaxima && \
	apt-get -y install libzmq3-dev gnuplot

WORKDIR /usr/local/src/

RUN wget --quiet --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://github.com/Clozure/ccl/releases/download/v1.11.5/ccl-1.11.5-linuxx86.tar.gz && \
	wget --quiet --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://beta.quicklisp.org/quicklisp.lisp && \
	wget --quiet --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 https://sourceforge.net/projects/maxima/files/Maxima-source/5.41.0-source/maxima-5.41.0.tar.gz/download -O maxima-5.41.0.tar.gz && \
	git clone --depth=1 http://github.com/robert-dodier/maxima-jupyter.git && \
	tar -xzf ccl-1.11.5-linuxx86.tar.gz && \
	tar -xzf maxima-5.41.0.tar.gz && \
	rm -f ccl-1.11.5-linuxx86.tar.gz && \
	rm -f maxima-5.41.0.tar.gz


COPY src/ /usr/local/src/scripts/

RUN cp --target-dir /usr/local/bin/ /usr/local/src/ccl/scripts/ccl64 /usr/local/src/ccl/scripts/ccl && \
	cat /usr/local/src/scripts/lisp/install_quicklisp.lisp | ccl64 --load quicklisp.lisp  && \
	cd maxima-5.41.0  && \
	cat /usr/local/src/scripts/lisp/configure_maxima.lisp | ccl64 --load ../quicklisp.lisp && \
	cd src && \
	cat /usr/local/src/scripts/lisp/build_maxima.lisp | ccl64 --load ../../quicklisp.lisp && \
	cat /usr/local/src/scripts/lisp/build_maxima-jupyter.lisp | ccl64  && \
	cp /usr/local/src/maxima-5.41.0/src/maxima-jupyter /usr/local/bin && \
	rm -rf /usr/local/src/ccl && \
	rm -rf usr/local/src/maxima-5.41.0

WORKDIR /usr/local/src/maxima-jupyter/
	
RUN python3 ./install-maxima-jupyter.py --maxima-jupyter-exec=/usr/local/bin/maxima-jupyter  && \
	install -D /usr/local/src/maxima-jupyter/maxima.js /opt/conda/lib/python3.6/site-packages/notebook/static/components/codemirror/mode/maxima/maxima.js && \
	patch /opt/conda/lib/python3.6/site-packages/notebook/static/components/codemirror/mode/meta.js /usr/local/src/maxima-jupyter/codemirror-mode-meta-patch && \
	install /usr/local/src/maxima-jupyter/maxima_lexer.py /opt/conda/lib/python3.6/site-packages/pygments/lexers/maxima_lexer.py && \
	patch /opt/conda/lib/python3.6/site-packages/pygments/lexers/_mapping.py /usr/local/src/maxima-jupyter/pygments-mapping-patch

USER jovyan

RUN mkdir /home/jovyan/notebooks
WORKDIR /home/jovyan/notebooks